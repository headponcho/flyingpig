﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnchor : MonoBehaviour {

    GameObject player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        transform.SetPositionAndRotation(new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z), transform.rotation);
    }
}
