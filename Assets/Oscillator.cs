﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {

    [SerializeField] Vector3 movementVector = new Vector3(10.0f, 10.0f, 10.0f);
    [SerializeField] float period = 2.0f;

    const float TAU = Mathf.PI * 2.0f;
    Vector3 startingPos;

    // Use this for initialization
    void Start () {
        startingPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (period <= Mathf.Epsilon) { return; }

        float cycles = Time.time / period; // grows continually from 0
        float movementFactor = Mathf.Sin(cycles * TAU) / 2.0f + 0.5f; // goes from 0 to 1

        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPos + offset;
	}
}
